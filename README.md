# Trigger Logger (ALPHA)
[![Github All Releases](https://img.shields.io/github/downloads/PhaxeNor/Trigger-Logger/total.svg?style=for-the-badge)](https://github.com/PhaxeNor/Trigger-Logger) [![Discord](https://img.shields.io/discord/469888814819835904.svg?style=for-the-badge&label=Discord)](https://discord.gg/3RBe8kB) [![Twitter Follow](https://img.shields.io/twitter/follow/phaxenor.svg?style=for-the-badge&label=Twitter)](https://twitter.com/PhaxeNor)

Trigger Logger is a Unity Editor script that will give you a list of all your VRChat triggers.

This was created to give you a better overview over all your triggers.

Current release is a alpha version and will contain errors and flaws. Please report these as you see them.

### Current Features:
- List over triggers
- Stats (Total Triggers and RPC)
- Export (Export your triggers to a text file)
- Filtering


Support and help is done on Discord only. Bug reports can be done here or on Discord.
